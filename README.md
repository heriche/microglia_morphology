## Data and code for the paper "Characterization of microglia behavior in healthy and pathological conditions with image analysis tools. Martinez A, Hériché JK, Calvo M, Tischer C, Otxoa-de-Amezaga A, Bosch A, Planas, AM and Petegnief V. [Open Biol. 2023. Open Biol.13:220200](https://doi.org/10.1098/rsob.220200)."

### To reproduce the analysis:
1. Download and uncompress this repository or clone it with `git clone https://git.embl.de/heriche/microglia_morphology.git`
2. Open the notebook morphology_classifier.Rmd with [RStudio](https://www.rstudio.com/)
3. Install required R packages. RStudio should offer to install packages it can't find.
4. Set the microglia_morphology project directory as working directory (from the menu Session > Set Working Directory > Choose Directory).
5. Click Run > Run All (or press Ctrl+Alt+R)

### To explore the data with the [Image Data Explorer](https://git.embl.de/heriche/image-data-explorer):
1. Navigate to https://shiny-portal.embl.de > Data analysis tools > Image Data Explorer
or [install the Image Data Explorer locally](https://git.embl.de/heriche/image-data-explorer/-/wikis/Installation).
2. As input data file, upload the file output/supervised/morphology_data_with_annotated_cells_and_predictions.txt produced by running the notebook (or use the file provided with the paper).
3. Populate some of the other input fields either by uploading the file IDE-saved_input_choices.rds into the 'Restore settings from file' section or by manually selecting/entering values in the fields.  
The images are in an S3 bucket hosted by the BioImage Archive with the following connection details:
	- S3 end point: uk1s3.embassy.ebi.ac.uk
	- Access key: (leave empty)
	- Secret key: (leave empty)
	- Region: (leave empty)
	- Bucket: bia-open-data
	
	
